\pagestyle{fancy}
\fancyhead{}
\fancyhead[L]{Deep Learning - Sheet 04}
<!-- \fancyhead[C]{} -->
\fancyhead[R]{\theauthor}

\hfill
\vspace{-0.5cm}

# Theory

## The following XAI methods are ...

1. No
2. Yes
3. No   - (no global explanations, rest is true)
4. \-


## The overarching idea behind the following XAI method is ...

1. Yes
2. Yes
3. Yes
4. Yes

## The following training/optimization algorithms are used to extract the respective explanations:

1. Yes
2. Yes
3. Yes
4. Yes

## The following explanation methods can be used (among other purproses) for the purpose of ...
1. Yes
2. Yes
3. Yes  - (plausible counterfactuals)
4. Yes  - (use it to find identifiers that cause wrong labels)

## The following statements are true:

1. Yes  - (take LIME)
2. No   - (only for some methods)
4. \-
3. No

\newpage
<!-- \begin{scriptsize}
All of our code for the exercises can be found as jupyter notebooks at \url{https://gitlab.ub.uni-bielefeld.de/astrathmann/dl22}
\end{scriptsize} -->

# Practical Part \scriptsize Code for the exercises as jupyter notebooks at \url{https://gitlab.ub.uni-bielefeld.de/astrathmann/dl22}

## Analysis of explanations

We used our model from the first sheet. It consists of two convolutional layers with max pooling and two linear layers, using ReLU activation at each step. We used ADAM for optimizations and the following parameters: Cross Entropy as Loss function, 0.001 as Learning Rate and 5 Epochs

\if false

\begin{table}[H]
\centering
\begin{tabular}{|l|l|l|}
\hline
Loss Function & Learning Rate & Number of epochs \\ \hline
Cross Entropy  & 0.001 & 5 \\ \hline
\end{tabular}
\end{table}

\fi

\begin{figure}
\begin{subfigure}{.5\textwidth}
    %\centering
    \RaggedRight{We used Saliency Maps to explain the predicted classification. \\You can see that in figure (a) the most important pixels for the class of dress are right by the sides of the dress. This is also the case for the Trowser and also for the worst class Sandal. But they have a much wider area an the intensity is also more distributed. We think that this are the most important pixels because most dress are slim on the pictures and don't have sleeves. \\In (b) for the class Trowser the most important pixels are the gap between the legs. Most as trowser classified images have a gap there, therefore it makes sense that it would be important. The class T-Shirt is the next best with 0.001 \% certainty. It has the gap as important pixel as well the areas around the trowsers. And for the least class the gap is also important, with some other areas in the roughly in a spherical pattern around the center of the image.}
    
\end{subfigure}%
\begin{subfigure}{.05\textwidth}
\makebox[0.03cm]{ }
\end{subfigure}%
\begin{subfigure}{.45\textwidth}
    \centering
    \begin{tabular}{@{}c@{}}
    \includegraphics[]{exercise_04/explanation_dress.png} \\[\abovecaptionskip]
    \small (a) Explanations for a image of a dress.
    \end{tabular}

    \vspace{\floatsep}

    \begin{tabular}{@{}c@{}}
    \includegraphics[]{exercise_04/explanation_trowser.png} \\[\abovecaptionskip]
    \small (b) Explanations for a image of a trowser.
    \end{tabular}
\end{subfigure}
% \caption{plots of....}
% \label{fig:fig}
\end{figure}

## Analysis of different explanation Methods

For this task we tried Saliency, Guided Backdrop, Integrated Gradients and Occlusion as explanation methods for a image of a bag. Saliency, Guided Backdrop and Integrated Gradients looked in roughly the same area with different intensities (\ref{fig:different_explanations_normal}). Saliency focused very intensly on the top area that is not part of the bag. Only Occlusion did focus mostly on the right area. The attacked image was classified as coat and had different explanations (\ref{fig:different_explanations_attack}). Most of the important areas for the classification are in the same area as before in Saliency, Guided Backdrop and Integrated Gradients explanations, but the intensities are different and the points are focused on the modified pixels. Now also the Occlusion is focused on the left area, but still mostly on the modified artifacts. So the added artefacts did alter the classification according to the explanations.

\begin{figure}[H]
\begin{subfigure}{.45\textwidth}
    \centering
    \includegraphics[width=.85\linewidth]{exercise_04/different_explanations_normal.png}
    \caption{Different explanations of the classification of a image of a bag.}
    \label{fig:different_explanations_normal}
\end{subfigure}%
\begin{subfigure}{.1\textwidth}
\makebox[0.1cm]{ }
\end{subfigure}%
\begin{subfigure}{.45\textwidth}
    \centering
    \includegraphics[width=.85\linewidth]{exercise_04/different_explanations_attacked.png} \\[\abovecaptionskip]
    \caption{Different explanations of the classification of a attacked image as coat.}
    \label{fig:different_explanations_attack}
\end{subfigure}
% \caption{plots of....}
% \label{fig:fig}
\end{figure}
