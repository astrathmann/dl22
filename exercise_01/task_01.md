\pagestyle{fancy}
\fancyhead{}
\fancyhead[L]{Deep Learning - Sheet 01 ||}
<!-- \fancyhead[C]{} -->
\fancyhead[R]{\theauthor}

\hfill
\vspace{-0.5cm}


<!-- ## Deep Learning Sheet 1 - Theory -->

# Theory

1.
    1. no (both are universal approx.)
    2. yes (with restrictions, some complications with gradient methods for DL - exploding & vanishing gradient problem, steep gradients & plateaus)
    3. yes
    4. yes

2.
    1. yes
    2. no (not a bad move but wrong category here) yes
    3. yes
    4. no (model characteristic unrelated to generalization or numeric behaviour) [?]

3.
    1. yes (but there are further conditions)
    2. no (afaik CNNs can't do that unless you "stack" them)
    3. yes
    4. yes

4.
    1. yes
    2. no (is for skeletal pose detection)
    3. no (could perhaps be used for the image recognition component, but isn't in the example from the lecture)
    4. yes

5.
    1. no (they use convolutions...) [?]
    2. nope (because you would need preprocessing)
    3. no (we want dimensionality reduction, not dimensionality bloat)
    4. yes

# Praxis

<!-- Beispiel für eine Grafik -->
\begin{figure}[H]
    \centering
    \includegraphics[width=0.4\textwidth]{exercise_01/input.jpg}
    \caption{Cat}
    \label{fig:cat}
\end{figure}