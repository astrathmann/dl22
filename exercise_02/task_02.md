\pagestyle{fancy}
\fancyhead{}
\fancyhead[L]{Deep Learning - Sheet 02}
<!-- \fancyhead[C]{} -->
\fancyhead[R]{\theauthor}

\hfill
\vspace{-0.5cm}

# Theory

## The following generative models rely on the specific objective, ...

1. No   - (no discriminator in VAE)
2. No   - (first part yes, second part no)
3. Yes
4. Yes


## For variational autoencoders, the following is true:

1. Yes
2. Yes
3. Yes
4. Yes

## The following holds for GANs and its variants:

1. No   - (uses Wasserstein distance)
2. No   - (they're generally not, hence why WS is used)
3. Yes  - (either is L-continuous or gets approximated)
4. Yes

## The following tasks can be addressed with generative models:

1. \-
2. Yes
3. No
4. Yes  - (e.g. DALL-E)

## The following is true:

1. Yes
2. No   - (that would imply the generator "lost")
4. Yes
3. Yes

\newpage
<!-- \begin{scriptsize}
All of our code for the exercises can be found as jupyter notebooks at \url{https://gitlab.ub.uni-bielefeld.de/astrathmann/dl22}
\end{scriptsize} -->

# Practical Part \scriptsize Code for the exercises as jupyter notebooks at \url{https://gitlab.ub.uni-bielefeld.de/astrathmann/dl22}

## Variational Autoencoder


We used a VAE with three linear layers for encoding and two linear layers for decoding. In the encoding stage we used a ReLu after the first linear layer and passed the output to both the other linear layers that are used as variance and mean encoder. This output is reparameterized and feed to the decoding stage. A second ReLu is used between both linear layers of the decoding stage. We used ADAM for optimizations and the following parameters:

\begin{table}[H]
\begin{tabular}{|l|l|l|l|l|}
\hline
Batch size & Latent dim & Number of epochs & Learning rate & Max saves per epoch \\ \hline
128  & 20 & 20 & 0.001 & 20 \\ \hline
\end{tabular}
\end{table}

The reconstruction error descended pretty quickly and stagnated as you can see in the following image:

\begin{figure}
\begin{subfigure}{.5\textwidth}
    \centering
    \includegraphics[width=1\textwidth]{exercise_02/reconstruction_error.png}
    \caption{Reconstruction Error}
    \label{fig:fashion_original}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
    \centering
    \begin{tabular}{@{}c@{}}
    \includegraphics[width=.7\linewidth,height=50pt]{exercise_02/reconstruction_difference_examples.png} \\[\abovecaptionskip]
    \small (b) Reconstruction examples
    \end{tabular}

    \vspace{\floatsep}

    \begin{tabular}{@{}c@{}}
    \includegraphics[width=.7\linewidth,height=50pt]{exercise_02/random_decode_examples.png} \\[\abovecaptionskip]
    \small (c) Random decoded images
    \end{tabular}
\end{subfigure}
% \caption{plots of....}
% \label{fig:fig}
\end{figure}

## Analysis of latent space

You can see in the UMAP visualisation, that there are three big clusters with some subclusters. The three big clusters are trowsers, footwear and all the other classes. But in the last cluster you can clearly see, that you have a subcluster with the bag label that is only loosly connectet to the bodywear labels. In those clusters you can differentiate the subclusters, but there is some overlay.

\begin{figure}[H]
\begin{subfigure}{.45\textwidth}
    \centering
    \includegraphics[]{exercise_02/line_in_UMAP_shoe_dress.png}
    \caption{UMAP of interpolation (line does belong to the interpolation)}
    \label{fig:interpolation_umap}
\end{subfigure}%
\begin{subfigure}{.05\textwidth}
\makebox[0.05cm]{ }
\end{subfigure}%
\begin{subfigure}{.50\textwidth}
    \centering
    \begin{tabular}{@{}c@{}}
    \includegraphics[width=1\linewidth,height=75pt]{exercise_02/interpolation_shoe_dress.png} \\[\abovecaptionskip]
    \small (b) Interpolation in the latent space
    \end{tabular}

    \vspace{\floatsep}

    \RaggedRight {We also interpolated a ankle boot to a dress. You can see that during the movement it went through the area of t-shirt labels in the latent space. This correlates to the UMAP visualisation.}
    
\end{subfigure}
% \caption{plots of....}
% \label{fig:fig}
\end{figure}
